/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Mauricio
 */
public class Square extends Rectangle{
    
    public double getArea(){
        return this.height * this.height;
    }
    
    public double getPerimeter(){
        return (this.height * 4);
    } 
}
