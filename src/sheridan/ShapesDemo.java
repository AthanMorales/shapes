/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Mauricio
 */
public class ShapesDemo {
    public static void calculateArea(Rectangle r){
        r.setWidth(2);
        r.setHeight(3);
        if(r.getArea() != 6){
            System.out.println("Area calculation is incorrect");
        }
    }
    
    public static void main(String[] args){
        ShapesDemo.calculateArea(new Rectangle() {});
        ShapesDemo.calculateArea(new Square());
    }    
}
